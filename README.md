# Foundry VTT 5th Edition

An implementation of the first edition of Pathfinder for Foundry Virtual
Tabletop (http://foundryvtt.com).

The software component of this system is distributed under the GNUv3 license
while the game content is distributed under the Open Gaming License v1.0a.

## Installation

Install the following game system in FoundryVTT's game system tab: [https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/raw/master/system.json](https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/raw/master/system.json)

If you wish to manually install the system, you must clone or extract it into
the Data/systems/pf1 folder. You may do this by cloning the repository or
downloading a zip archive from the [Releases Page](https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/releases).

## Information

You can view information on this game system [here](https://furyspark.gitlab.io/foundryvtt-pathfinder1-doc/).

